import CustomElement from './components/custom-element';
import Footer from './components/footer';
import Header from './components/header';
import './components/component.scss';

function App() {
  return (
    <div className="container">
      <Header />
      <CustomElement />
      <Footer />
    </div>
  );
}

export default App;
