/* eslint-disable import/no-anonymous-default-export */
const initialState = {
    backGroundColor: '',
    colorHexaValue: []
}

export default function (state = initialState, action) {
    console.log("Redux Value", action.payload)
    switch (action.type) {
        case 'backGroundColor':
            return {
                ...state,
                backGroundColor: action.payload,
            }
        case 'colorHexaValue':
            return {
                ...state,
                colorHexaValue: action.payload,
            }
        default: return state;
    }
}

