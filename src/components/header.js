import React from 'react';
import './component.scss';

class Header extends React.Component {

  render() {

    return (
      <div className="header">
        <h1 style={{ textAlign: 'center' }}>Custom Element </h1>
      </div>
    )
  }
}

export default (Header)
