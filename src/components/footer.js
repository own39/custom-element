import React from 'react';
import './component.scss';

class Footer extends React.Component {

  render() {

    return (
      <div className="footer">
        <p style={{ textAlign: 'center' }}>Developed By Milind Sewak</p>
      </div>
    )
  }
}

export default (Footer)
