import React from 'react';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import Switch from '@material-ui/core/Switch';
import './component.scss';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { setBackGroundColor, setColorHexaValueList } from '../action/customColorAction';


class CustomElement extends React.Component {

    state = {
        hexValueList: ['#5e4fdb', '#F3F2FC', '#1ABC9C', '#D64B4B', '#852121',
            '#FFE3E3', '#C06801', '#FFF7E3', '#0E4316', '#E1EFE1', '#000000', '#FFFFFF', '#808080', '#D8D8D8', '#F0F0F0', '#F6F6F6']
    }

    constructor(props) {
        super(props)
    }
    componentWillMount() {
        this.props.setColorHexaValueList(this.state.hexValueList);
    }
    componentDidUpdate() {

        document.documentElement.style.setProperty("--changeColor", this.props.backGroundColor);
    }

    changeCol = (value) => {
        console.log(this.props.colorHexaValue);
        this.props.setBackGroundColor(value);
    };

    render() {

        return (

            <div className="tabs-container-scroll">
                {
                    this.props.colorHexaValue &&
                    this.props.colorHexaValue.map((data, index) => {
                        return (
                            <div className="tabs-col">
                                <Button id={'b' + (index + 1)} variant="contained" onClick={() => this.changeCol(data)}>  Color {index + 1}   </Button>
                                <Checkbox
                                    id={'b' + (index + 1)}
                                    onClick={() => this.changeCol(data)}
                                />
                                <Switch id={'b' + (index + 1)} onClick={() => this.changeCol(data)} />
                            </div>
                        )
                    })
                }
            </div >
        );
    }
}

CustomElement.propTypes = {
    setBackGroundColor: PropTypes.func.isRequired,
    setColorHexaValueList: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
    backGroundColor: state.backGroundColor,
    colorHexaValue: state.colorHexaValue
});

export default connect(mapStateToProps, { setBackGroundColor, setColorHexaValueList })(CustomElement)
