export function setBackGroundColor(hexValue) {

    return function (dispatch) {
        dispatch({
            type: 'backGroundColor',
            payload: hexValue,
        })
    }
}

export function setColorHexaValueList(hexValueList) {

    return function (dispatch) {
        dispatch({
            type: 'colorHexaValue',
            payload: hexValueList,
        })
    }
}

