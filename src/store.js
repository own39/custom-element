import { createStore, applyMiddleware } from 'redux';
import customColorReducer from './reducer/customColorReducer';
import thunk from 'redux-thunk';

const initialState = {};
const middleware = [thunk];
const store = createStore(
    customColorReducer,
    initialState,
    applyMiddleware(...middleware)
);
export default store;